package org.example;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт, " + getFamily().getPet().getNickname() + ". Ти дуже гарний!");
    }

    public void repairCar() {
        System.out.println("Треба перевірити авто.");
    }
}
