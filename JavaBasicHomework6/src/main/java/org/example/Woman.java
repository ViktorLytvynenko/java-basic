package org.example;

public final class Woman extends Human {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт, " + getFamily().getPet().getNickname() + ". Ти дуже гарний!");
    }

    public void makeup() {
        System.out.println("Треба підфарбуватися.");
    }
}
