package org.example;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
/*      Написати програму "числа", яка загадує випадкове число та пропонує гравцеві його вгадати.
        Технічні вимоги:

        За допомогою java.util.Random програма загадує випадкове число в діапазоні[0 - 100] та пропонує гравцеві через
        консоль ввести своє ім 'я, яке зберігається в змінній name.
        Перед початком на екран виводиться текст:Let the game begin !
                Сам процес гри обробляється у нескінченному циклі.
        Гравцеві пропонується ввести число в консоль, після чого програма порівнює загадану кількість з тим, що
        ввів користувач.
        Якщо введене число менше загаданого, програма виводить на екран текст:Your number is too small.Please,try again..
        Якщо введене число більше за загадане, то програма виводить на екран текст:
        Your number is too big.Please,try again..
        Якщо введене число відповідає загаданому, то програма виводить текст:
        Congratulations, {name} !
                Завдання повинно бути виконане за допомогою масиви (НЕ використовуйте інтерфейси List, Set, Map).

        Необов 'язкове завдання просунутої складності:

        1. Перед переходом на наступну ітерацію, програма зберігає введене користувачем число масив.Після того, як
        гравець вгадав загадане число, перед виходом програма виводить на екран текст:Your numbers:і показує всі введені
        гравцем числа, відсортовані від більшого до меншого.
        2. Після введення чисел користувачем додати перевірку їх коректності.Якщо користувач запровадив не число - запитати
        заново.
        3. Додайте грі трохи більше сенсу:нехай загадане число буде роком, якому відповідає відома
        подія.Інформація про роки зберігається у двовимірному масиві[рік Х подія].Програма спочатку вибирає випадковим
        чином комірку в матриці й виводить на екран:When did the World War II begin ?*/
        Random random = new Random();
        byte randomNumber = (byte) random.nextInt(101);

        Scanner in = new Scanner(System.in);
        System.out.println("Write please your name ");
        String name = in.nextLine();

        System.out.println("Let the game begin!");
        System.out.println("Write please number from 0 to 100 ");
        byte[] allUserNumbers = new byte[0];

        while (true) {
            while (!in.hasNextByte()) {
                System.out.println("Write please number from 0 to 100 ");
                in.next();
            }
            byte userNumber = in.nextByte();

            byte[] tempAllUserNumbers = new byte[allUserNumbers.length + 1];
            System.arraycopy(allUserNumbers, 0, tempAllUserNumbers, 0, allUserNumbers.length);
            tempAllUserNumbers[allUserNumbers.length] = userNumber;
            allUserNumbers = tempAllUserNumbers;

            if (randomNumber > userNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (randomNumber < userNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations " + name + "!");
                break;
            }
        }
        Arrays.sort(allUserNumbers);
        System.out.println("Your numbers:");
        for (int i = allUserNumbers.length - 1; i >= 0; i--) {
            System.out.print(allUserNumbers[i] + " ");
        }
    }
}