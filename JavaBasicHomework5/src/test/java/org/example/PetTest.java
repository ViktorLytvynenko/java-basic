package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @org.junit.jupiter.api.Test
    void testToString() {
        Pet dog = new Pet(Species.DOG, "Rock", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        String ExpectedResult = "DOG{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep], canFly=false, numberOfLegs=4, hasFur=true}";
        assertEquals(ExpectedResult, dog.toString(), "They are not equal");
    }

    @Test
    void testEquals() {
        Pet dog1 = new Pet(Species.DOG, "Rock", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        Pet dog2 = new Pet(Species.DOG, "Rock", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        Pet dog3 = new Pet(Species.DOG, "Rambo", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        assertEquals(dog1, dog2);
        assertNotEquals(dog3, dog1);
    }

    @Test
    void testHashCode() {
        Pet dog1 = new Pet(Species.DOG, "Rock", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        Pet dog2 = new Pet(Species.DOG, "Rock", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        assertEquals(dog1.hashCode(), dog2.hashCode());
    }
}