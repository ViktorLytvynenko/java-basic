package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testToString() {
        Human fatherDexter = new Human("John", "Dexter", 1900, 80, new String[][]{
                {DayOfWeek.MONDAY.name(), "go to courses"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "go for a walk"},
                {DayOfWeek.THURSDAY.name(), "go SPA"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "watch a film"},
                {DayOfWeek.SUNDAY.name(), "do home work"},
        });
        String ExpectedResult = "Human{name='John', surname='Dexter', year=1900, iq=80, schedule=[[MONDAY, go to courses], [TUESDAY, go to gym], [WEDNESDAY, go for a walk], [THURSDAY, go SPA], [FRIDAY, visit friends], [SATURDAY, watch a film], [SUNDAY, do home work]]}";
        assertEquals(ExpectedResult, fatherDexter.toString(), "They are equal");
    }

    @Test
    void testEquals() {
        Human fatherDexter = new Human("John", "Dexter", 1900, 80, new String[][]{
                {DayOfWeek.MONDAY.name(), "go to courses"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "go for a walk"},
                {DayOfWeek.THURSDAY.name(), "go SPA"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "watch a film"},
                {DayOfWeek.SUNDAY.name(), "do home work"},
        });
        Human fatherDexter2 = new Human("John", "Dexter", 1900, 80, new String[][]{
                {DayOfWeek.MONDAY.name(), "go to courses"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "go for a walk"},
                {DayOfWeek.THURSDAY.name(), "go SPA"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "watch a film"},
                {DayOfWeek.SUNDAY.name(), "do home work"},
        });
        assertEquals(fatherDexter2, fatherDexter, "They are equal");
        Human fatherDexter3 = new Human("Petya", "Dexter", 1900, 80, new String[][]{
                {DayOfWeek.MONDAY.name(), "go to courses"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "go for a walk"},
                {DayOfWeek.THURSDAY.name(), "go SPA"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "watch a film"},
                {DayOfWeek.SUNDAY.name(), "do home work"},
        });
        assertNotEquals(fatherDexter3, fatherDexter, "They are not equal");
    }

    @Test
    void testHashCode() {
        Human fatherDexter = new Human("John", "Dexter", 1900, 80, new String[][]{
                {DayOfWeek.MONDAY.name(), "go to courses"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "go for a walk"},
                {DayOfWeek.THURSDAY.name(), "go SPA"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "watch a film"},
                {DayOfWeek.SUNDAY.name(), "do home work"},
        });
        Human fatherDexter2 = new Human("John", "Dexter", 1900, 80, new String[][]{
                {DayOfWeek.MONDAY.name(), "go to courses"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "go for a walk"},
                {DayOfWeek.THURSDAY.name(), "go SPA"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "watch a film"},
                {DayOfWeek.SUNDAY.name(), "do home work"},
        });
        assertEquals(fatherDexter2.hashCode(), fatherDexter.hashCode(), "They are equal");
    }
}