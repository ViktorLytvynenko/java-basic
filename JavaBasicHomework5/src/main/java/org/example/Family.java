package org.example;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    static {
        System.out.println("Loading new Class Family");
    }

    {
        System.out.println("Creating new Family Object");
    }

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        if (mother == null || father == null) {
            throw new IllegalArgumentException("Must be two parents");
        }
        this.mother = mother;
        this.father = father;

        this.mother.setFamily(this);
        this.father.setFamily(this);

        this.children = new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children.clone();
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] updatedChildren = Arrays.copyOf(children, children.length + 1);
        updatedChildren[updatedChildren.length - 1] = child;
        children = updatedChildren;
    }

    public boolean deleteChild(Human child) {
        int index = -1;

        for (int i = 0; i < this.children.length; i++) {
            if (this.children[i].equals(child)) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            System.arraycopy(this.children, index + 1, this.children, index, this.children.length - 1 - index);
            child.setFamily(null);
            return true;
        }
        return false;
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= this.children.length)
            return false;
        Human childToDelete = this.children[index];
        Human[] newChildren = new Human[this.children.length - 1];
        System.arraycopy(this.children, 0, newChildren, 0, index);
        System.arraycopy(this.children, index + 1, newChildren, index, this.children.length - 1 - index);
        children = newChildren;
        childToDelete.setFamily(null);
        return true;
    }

    public int countFamily() {
        return children.length + 2;
    }

    @Override
    public String toString() {
        return "Family{mother=" + this.mother + ", father=" + this.father + ", children=" + Arrays.toString(this.children) + ", pet=" + this.pet + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    protected void finalize() throws Throwable {
        System.out.println("Deleting from class Family object " + this);
        super.finalize();
    }
}