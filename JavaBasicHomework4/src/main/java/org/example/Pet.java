package org.example;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    static {
        System.out.println("Loading new Class Pet");
    }
    {
        System.out.println("Creating new Pet Object");
    }
    private String species;
    private String nickname;
    private byte age;
    private byte trickLevel;
    private String[] habits;

    public void eat() {
        System.out.println("Я ї'м!");
    }

    public void respond(String _nickname) {
        System.out.println("Привіт, хазяїн. Я - " + _nickname + ". Я скучив!");
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    private static boolean validateTrickLevel(byte trickLevel) {
        return trickLevel >= 0 && trickLevel <= 100;
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Pet(String species, String nickname, byte age, byte trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        if (Pet.validateTrickLevel(trickLevel))
            this.trickLevel = trickLevel;
        else throw new IllegalArgumentException("It must be a number from 0 to 100");
        this.habits = habits;
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public byte getAge() {
        return age;
    }

    public byte getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public void setTrickLevel(byte trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits.clone();
    }

    @Override
    public String toString() {
        return this.species + "{nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return species == pet.species &&
                nickname == pet.nickname &&
                age == pet.age &&
                trickLevel == pet.trickLevel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }
}
