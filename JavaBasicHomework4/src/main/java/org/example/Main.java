package org.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("--------------------------------------------------------------------------------------------------------");
        Pet dog = new Pet("dog", "Rock", (byte) 5, (byte) 75, new String[]{"eat", "drink", "sleep"});
        System.out.println(dog);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        Human fatherDexter = new Human("John", "Dexter", 1900, 80, new String[][]{{"Sunday", "Vacation"}});
        System.out.println(fatherDexter);
        Human motherDexter = new Human("Samantha", "Dexter", 1900, 75, new String[][]{{"Sunday", "Vacation"}});
        System.out.println(motherDexter);
        Human child1Dexter = new Human("Alex", "Dexter", 1930, 55, new String[][]{{"Sunday", "Vacation"}});
        System.out.println(child1Dexter);
        Human child2Dexter = new Human("Andrew", "Dexter", 1935, 50, new String[][]{{"Sunday", "Vacation"}});
        System.out.println(child2Dexter);
        Family family1 = new Family(motherDexter, fatherDexter);
        family1.setPet(dog);
        family1.addChild(child1Dexter);
        family1.addChild(child2Dexter);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        System.out.println("After adding children");
        System.out.println(family1);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        family1.deleteChild(child2Dexter);
        System.out.println("After deleting child");
        System.out.println(family1);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        child1Dexter.greetPet();
        child1Dexter.describePet();
        child1Dexter.feedPet(true);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        Human fatherBole = new Human("Jack", "Bole", 1900, 80, new String[][]{{"Sunday", "Sunbath"}});
        System.out.println(fatherBole);
        Human motherBole = new Human("Vera", "Bole", 1900, 85, new String[][]{{"Sunday", "Sunbath"}});
        System.out.println(motherBole);
        Human child1Bole = new Human("Jacob", "Bole", 1930, 45, new String[][]{{"Sunday", "Sunbath"}});
        System.out.println(child1Bole);
        Human child2Bole = new Human("Kate", "Bole", 1935, 40, new String[][]{{"Sunday", "Sunbath"}});
        System.out.println(child2Bole);
        Family family2 = new Family(motherBole, fatherBole);
        family2.addChild(child1Bole);
        family2.addChild(child2Bole);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        System.out.println("After adding children");
        System.out.println(family2);
        System.out.println("--------------------------------------------------------------------------------------------------------");
        family1.deleteChild(child2Bole);
        System.out.println("After deleting child");
        System.out.println(family2);
    }
}