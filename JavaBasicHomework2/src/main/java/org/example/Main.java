package org.example;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random random = new Random();

        char[][] board = new char[6][6];
        int hitsCounter = 0;

        for (byte i = 0; i < board.length; i++) {
            board[0][i] = (char) ('0' + i);
        }
        for (byte i = 0; i < board.length; i++) {
            board[i][0] = (char) ('0' + i);
        }
        for (byte i = 1; i < board.length; i++) {
            for (byte j = 1; j < board[i].length; j++) {
                board[i][j] = '-';
            }
        }

        byte x = (byte) random.nextInt(1, 5);
        byte y = (byte) random.nextInt(1, 5);

        String[] directions = new String[]{"left", "right", "up", "down"};
        int[][] coords = new int[3][2];

        do {
            int index = random.nextInt(directions.length);
            String direction = directions[index];

            if (direction.equals("left")) {
                if (y - 2 >= 1) {
                    coords[0] = new int[]{x, y};
                    coords[1] = new int[]{x, y - 1};
                    coords[2] = new int[]{x, y - 2};
                    break;
                }
            } else if (direction.equals("right")) {
                if (y + 2 < board[0].length) {
                    coords[0] = new int[]{x, y};
                    coords[1] = new int[]{x, y + 1};
                    coords[2] = new int[]{x, y + 2};
                    break;
                }
            } else if (direction.equals("up")) {
                if (x - 2 > 0) {
                    coords[0] = new int[]{x, y};
                    coords[1] = new int[]{x - 1, y};
                    coords[2] = new int[]{x - 2, y};
                    break;
                }
            } else {
                if (x + 2 < board.length) {
                    coords[0] = new int[]{x, y};
                    coords[1] = new int[]{x + 1, y};
                    coords[2] = new int[]{x + 2, y};
                    break;
                }
            }
        } while (true);

        System.out.println("---------------------------------");
        System.out.println("All Set. Get ready to rumble!");
        System.out.println("---------------------------------");
        System.out.println("It must be a number from 1 to 5");
        System.out.println("---------------------------------");

        while (true) {
            System.out.println("Enter horizontal target");

            while (!in.hasNextByte()) {
                System.out.println("Enter valid target");
                System.out.println("It must be a number from 1 to 5");
                in.next();
            }

            byte horizontalTarget = in.nextByte();

            if (horizontalTarget < 1 || horizontalTarget > 5) {
                System.out.println("Enter valid target");
                System.out.println("It must be a number from 1 to 5");

            } else {
                System.out.println("Enter vertical target");

                while (!in.hasNextByte()) {
                    System.out.println("Enter valid target");
                    System.out.println("It must be a number from 1 to 5");
                    in.next();
                }

                byte verticalTarget = in.nextByte();
                if (verticalTarget < 1 || verticalTarget > 5) {
                    System.out.println("Enter valid target");
                    System.out.println("---------------------------------");
                    System.out.println("It must be a number from 1 to 5");
                } else {
                    boolean isHit = false;
                    for (int[] elements : coords) {
                        int row = elements[0];
                        int col = elements[1];
                        if (board[row][col] != 'x' && horizontalTarget == row && verticalTarget == col) {
                            System.out.println("Good shot! You hit it!");
                            board[verticalTarget][horizontalTarget] = 'х';
                            hitsCounter++;
                            isHit = true;
                            System.out.println(hitsCounter);
                        }
                    }
                    if (!isHit) {
                        board[verticalTarget][horizontalTarget] = '*';
                        System.out.println("---------------------------------");
                        System.out.println("Nice try! Try again!");
                        System.out.println("---------------------------------");
                    }
                    for (byte i = 0; i < board.length; i++) {
                        for (byte j = 0; j < board[i].length; j++) {
                            System.out.print(board[i][j]);
                            System.out.print((char) 124);
                        }
                        System.out.println();
                    }
                    if (hitsCounter == 3) {
                        System.out.println("Good shot! You have won!");
                        break;
                    }
                }
            }
        }
    }
}