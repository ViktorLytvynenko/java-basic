package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String[][] scedule = new String[7][2];

        scedule[0][0] = "Monday";
        scedule[1][0] = "Tuesday";
        scedule[2][0] = "Wednesday";
        scedule[3][0] = "Thursday";
        scedule[4][0] = "Friday";
        scedule[5][0] = "Saturday";
        scedule[6][0] = "Sunday";
        scedule[0][1] = "go to courses";
        scedule[1][1] = "go to gym";
        scedule[2][1] = "go for a walk";
        scedule[3][1] = "go SPA";
        scedule[4][1] = "visit friends";
        scedule[5][1] = "watch a film";
        scedule[6][1] = "do home work";

        while (true) {
            System.out.println("Please, input the day of the week:");
            String userDayOfWeek = in.nextLine().trim().toLowerCase();

            switch (userDayOfWeek) {

                case ("monday"):
                    System.out.println("Your tasks for " + scedule[0][0] + ": " + scedule[0][1]);
                    break;
                case ("tuesday"):
                    System.out.println("Your tasks for " + scedule[1][0] + ": " + scedule[1][1]);
                    break;
                case ("wednesday"):
                    System.out.println("Your tasks for " + scedule[2][0] + ": " + scedule[2][1]);
                    break;
                case ("thursday"):
                    System.out.println("Your tasks for " + scedule[3][0] + ": " + scedule[3][1]);
                    break;
                case ("friday"):
                    System.out.println("Your tasks for " + scedule[4][0] + ": " + scedule[4][1]);
                    break;
                case ("saturday"):
                    System.out.println("Your tasks for " + scedule[5][0] + ": " + scedule[5][1]);
                    break;
                case ("sunday"):
                    System.out.println("Your tasks for " + scedule[6][0] + ": " + scedule[6][1]);
                    break;
                case ("change monday"):
                case ("reschedule monday"):
                    System.out.println("Please, input new tasks for " + scedule[0][0] + ".");
                    String userUpdatedMonday = in.nextLine().toLowerCase();
                    scedule[0][1] = userUpdatedMonday;
                    System.out.println("new tasks for " + scedule[0][0] + " saved!");
                    break;
                case ("change tuesday"):
                case ("reschedule tuesday"):
                    System.out.println("Please, input new tasks for " + scedule[1][0] + ".");
                    String userUpdatedTuesday = in.nextLine().toLowerCase();
                    scedule[1][1] = userUpdatedTuesday;
                    System.out.println("new tasks for " + scedule[1][0] + " saved!");
                    break;
                case ("change wednesday"):
                case ("reschedule wednesday"):
                    System.out.println("Please, input new tasks for " + scedule[2][0] + ".");
                    String userUpdatedWednesday = in.nextLine().toLowerCase();
                    scedule[2][1] = userUpdatedWednesday;
                    System.out.println("new tasks for " + scedule[2][0] + " saved!");
                    break;
                case ("change thursday"):
                case ("reschedule thursday"):
                    System.out.println("Please, input new tasks for " + scedule[3][0] + ".");
                    String userUpdatedThursday = in.nextLine().toLowerCase();
                    scedule[3][1] = userUpdatedThursday;
                    System.out.println("new tasks for " + scedule[3][0] + " saved!");
                    break;
                case ("change friday"):
                case ("reschedule friday"):
                    System.out.println("Please, input new tasks for " + scedule[4][0] + ".");
                    String userUpdatedFriday = in.nextLine().toLowerCase();
                    scedule[4][1] = userUpdatedFriday;
                    System.out.println("new tasks for " + scedule[4][0] + " saved!");
                    break;
                case ("change saturday"):
                case ("reschedule saturday"):
                    System.out.println("Please, input new tasks for " + scedule[5][0] + ".");
                    String userUpdatedSaturday = in.nextLine().toLowerCase();
                    scedule[5][1] = userUpdatedSaturday;
                    System.out.println("new tasks for " + scedule[5][0] + " saved!");
                    break;
                case ("change sunday"):
                case ("reschedule sunday"):
                    System.out.println("Please, input new tasks for " + scedule[6][0] + ".");
                    String userUpdatedSunday = in.nextLine().toLowerCase();
                    scedule[6][1] = userUpdatedSunday;
                    System.out.println("new tasks for " + scedule[6][0] + " saved!");
                    break;
                case ("exit"):
                    return;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.;");
                    break;
            }
        }
    }
}