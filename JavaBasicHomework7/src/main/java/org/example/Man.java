package org.example;

import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, int year, Pet pet) {
        super(name, surname, year, pet);
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }

    @Override
    public void greetPet() {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                System.out.println("Привіт, " + getFamily().findOnePet(pet).getNickname() + ". Ти дуже гарний!!!");
            }
        } else {
            System.out.println("no pets");
        }
    }

    public void repairCar() {
        System.out.println("Треба перевірити авто.");
    }
}
