package org.example;

import org.example.Controllers.FamilyController;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileManager {
    private final static String FILE_PATH_DATA = "Data/data.dat";
    private final static String FILE_PATH_TEST = "Data/test.dat";

    public static void writeData(List<Family> families) throws IOException {
        Files.createDirectories(Paths.get("Data/"));
        try (FileOutputStream fileOut = new FileOutputStream(FILE_PATH_DATA);
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
            objectOut.writeObject(families);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Family> loadTestData() throws IOException {
        Files.createDirectories(Paths.get("Data/"));
        List<Family> families = new ArrayList<>();
        if (Files.exists(Paths.get(FILE_PATH_TEST))) {
            try (FileInputStream fileIn = new FileInputStream(FILE_PATH_TEST);
                 ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
                families = (List<Family>) objectIn.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        return families;
    }

    public static List<Family> loadData() throws IOException {
        Files.createDirectories(Paths.get("Data/"));
        List<Family> families = new ArrayList<>();
        if (Files.exists(Paths.get(FILE_PATH_DATA))) {
            try (FileInputStream fileIn = new FileInputStream(FILE_PATH_DATA);
                 ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
                families = (List<Family>) objectIn.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return families;
    }
}
